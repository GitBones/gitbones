# GitBones Template Request

## Name

*Provide a unique short name for the Template*

> Example: "Rolston Git CI Template"

## URL

*Provide the HTTPS URL for the repository. Note at this time GitBones only supports publicly available repos*

> Example: "https://gitlab.com/GitBones/templates/gitlab-starter.git"

## Description

*Provide a brief description for the template*

> Example: "GitLab starter template with description templates"

## Maintainer"

*Provide a reference of the team maintaining the template repository*

> Example: "GitBones Team"

## Author

** Provide the creator of the template. In some cases the team maintainer the template is not the same as the author. Give credit where credit is due **

> Example: "Rolston"

## GitHost

*Provide the intended Git remote server for the project. If no specific Git remote server required use "any"*

> Example: "GitLab"

## CI

*Provide the intended CI system for the project. If CI is not used simply use "none"*

> Example: "Gitlab CI"

## Topic

*Provide the general topic of the Template. If no specific topic identified simply use "General"* 

> Example: "AWS QuickStart"

## Language"

*Provide the primary programming language, syntax or general code used in the template. If no specific programming language identified simply use "none"*

> Example: "Python"

## Submodules

*Provide true/false if submodules are used in the template*

> Example: false