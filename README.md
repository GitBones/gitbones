# GitBones

GitBones is a git project templating service allowing quick project scaffolding to promote consistency and manageability across environments.


## GitBones Application

The GitBones application is currently supported in a PowerShell Core module that is supported on Windows and Mac OSs running PS Core 6.0 or greater. Installation can be through the PowerShell Gallery running `install-module GitBones`. Visit the Project at [https://gitlab.com/GitBones/psgitbones](https://gitlab.com/GitBones/psgitbones).

## Submitting Templates

To submit a Git Project template into GitBones, simply create an [Issue](https://gitlab.com/GitBones/gitbones/issues/new) using the `BonesRequest` template requesting your Git Project be added. 

Currently GitBones only supports public projects; however, if your project is hosted on GitHub or GitLab and is publicly available you can submit without any cost or obligation.
